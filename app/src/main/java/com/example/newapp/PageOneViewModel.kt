package com.example.newapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.newapp.data.response.request.GETWeatherData
import com.example.newapp.statusClass.WeatherStatus
import com.example.newapp.utils.addcomp
import com.example.newapp.model.RepositoryInterFace
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class PageOneViewModel(private val dataRepository: RepositoryInterFace) : BaseViewModel() {
    private val weatherStatus = MutableLiveData<WeatherStatus>()
    val weatherLiveData: LiveData<WeatherStatus>
        get() = weatherStatus

    fun fetchData(weatherData: GETWeatherData) {
        dataRepository.fetchWeatherData(weatherData).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.isSuccessful){
                    it.body()?.let {
                        weatherStatus.value = WeatherStatus.Success(it)
                    }
                }else weatherStatus.value = WeatherStatus.Failure(it.errorBody())
            }, {
                weatherStatus.value = WeatherStatus.TimeOut("${it.message}")
            }).addcomp(compositeDisposable)
    }
}