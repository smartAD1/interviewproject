package com.example.newapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_page2.*

class Page2Activity : AppCompatActivity() {
    private var startTime = ""
    private var endTime = ""
    private var weather = ""
    private var weatherTwo = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page2)
        intent?.let {
            startTime = it.getStringExtra("startTime") ?: ""
            endTime = it.getStringExtra("endTime") ?: ""
            weather = it.getStringExtra("weather") ?: ""
            weatherTwo = it.getStringExtra("weatherTwo") ?: ""
        }
        pageTwoStartTime.text = startTime
        pageTwoEndTime.text = endTime
        pageTwoWeather.text = String.format(getString(R.string.weather),weather,weatherTwo)
    }
}
