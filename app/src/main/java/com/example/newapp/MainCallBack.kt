package com.example.newapp

import com.example.newapp.data.response.Time


interface MainCallBack {

    fun gotoNextPage(time: Time)
}