package com.example.newapp

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.newapp.model.DataRepository

class ViewModelFactory: ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        with(modelClass) {
             when {
                isAssignableFrom(PageOneViewModel::class.java) -> PageOneViewModel(
                    dataRepository = DataRepository()
                )
                 else -> throw IllegalArgumentException(
                     "Unknown ViewModel class: ${modelClass.name}")
             }

        }as T
}