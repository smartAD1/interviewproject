package com.example.newapp

import android.app.Application
import io.reactivex.plugins.RxJavaPlugins



class RxApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        RxJavaPlugins.setErrorHandler { throwable: Throwable? ->
            throw Exception("$throwable")
        }
    }
}