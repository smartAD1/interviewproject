package com.example.newapp.utils

infix fun String.merge(string: String): String {
    return "${this}$string"
}

typealias ClickHandler = (String, String) -> Unit

