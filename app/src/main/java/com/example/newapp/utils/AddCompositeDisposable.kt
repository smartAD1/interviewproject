package com.example.newapp.utils

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


fun Disposable.addcomp(compositeDisposable: CompositeDisposable) {
    compositeDisposable.add(this)
}