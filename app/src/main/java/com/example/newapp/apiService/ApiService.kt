package com.example.newapp.apiService

import com.example.newapp.data.response.WeatherResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
//{Authorization}&format=JSON&locationName=臺北市&elementName=Mint}
interface ApiService {
        @GET("F-C0032-001/")
        fun fetchData(@Query("Authorization") auth: String,@Query("locationName")
        locationName: String,@Query("elementName")elementName: String): Single<Response<WeatherResponse>>
}