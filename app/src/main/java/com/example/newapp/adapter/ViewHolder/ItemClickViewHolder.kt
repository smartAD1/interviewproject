package com.example.newapp.adapter.ViewHolder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.newapp.data.response.Time
import com.example.newapp.MainCallBack
import com.example.newapp.R
import kotlinx.android.synthetic.main.item_click.view.*
import java.util.ArrayList

class ItemClickViewHolder(view: View): RecyclerView.ViewHolder(view) {
    fun setData(data: ArrayList<Time>, position: Int,
        callBack: MainCallBack) = itemView.apply {
        data[position]?.let {
            startDay.text = it.startTime
            endDay.text = it.endTime
            weather.text = String.format(context.getString(R.string.weather),it.parameter.parameterName,it.parameter.parameterUnit)
        }
        setOnClickListener {
            callBack.gotoNextPage(data[position])
        }
    }
}