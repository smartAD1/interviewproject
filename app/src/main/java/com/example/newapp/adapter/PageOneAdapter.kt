package com.example.newapp.adapter.ViewHolder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.newapp.data.response.Time
import com.example.newapp.MainCallBack
import com.example.newapp.R

class PageOneAdapter(private val callBack: MainCallBack): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var data = ArrayList<Time>()

    override fun getItemViewType(position: Int): Int =
        if (data[position].isIcon) 1 else 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when(viewType) {
            0 -> ItemClickViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_click,parent,false))
            1 -> ImageNoClickViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_no_click,parent,false))
            else -> ImageNoClickViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_no_click,parent,false))
        }



    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
       when(holder) {
           is ItemClickViewHolder -> holder.setData(data, position,callBack)
           is ImageNoClickViewHolder -> {
               holder.itemView.setOnClickListener {

               }
           }
       }
    }
}