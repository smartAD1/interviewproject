package com.example.newapp.statusClass

sealed class WeatherStatus {
    data class Success<T>(val data: T) : WeatherStatus()
    data class Failure<T>(val message: T) : WeatherStatus()
    data class TimeOut<T>(val message: T) : WeatherStatus()
}