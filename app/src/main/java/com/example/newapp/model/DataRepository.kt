package com.example.newapp.model

import com.example.newapp.apiService.RetrofitManager
import com.example.newapp.data.response.request.GETWeatherData
import com.example.newapp.data.response.WeatherResponse
import io.reactivex.Single
import retrofit2.Response

class DataRepository:RepositoryInterFace {

    override fun fetchWeatherData(weatherData: GETWeatherData): Single<Response<WeatherResponse>> {
        return RetrofitManager.getRetrofit().fetchData(auth = weatherData.apiKey,locationName = weatherData.locationName,
            elementName = weatherData.elementName)
    }
}