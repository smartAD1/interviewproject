package com.example.newapp.model

import com.example.newapp.data.response.request.GETWeatherData
import com.example.newapp.data.response.WeatherResponse
import io.reactivex.Single
import retrofit2.Response

interface RepositoryInterFace {
    
    fun fetchWeatherData(weatherData: GETWeatherData): Single<Response<WeatherResponse>>
}