package com.example.newapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.newapp.adapter.ViewHolder.PageOneAdapter
import com.example.newapp.data.response.request.GETWeatherData
import com.example.newapp.data.response.WeatherResponse
import com.example.newapp.lambda.myLamd
import com.example.newapp.statusClass.WeatherStatus
import com.example.newapp.utils.merge
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainCallBack {
    private val pageOneViewModel: PageOneViewModel by lazy {
        ViewModelProvider(this, ViewModelFactory()).get(PageOneViewModel::class.java)
    }
    private val pageOneAdapter: PageOneAdapter by lazy {
        PageOneAdapter(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        weatherList.layoutManager = LinearLayoutManager(this)

        val getWeatherData = GETWeatherData(
            apiKey = "CWB-4D757E58-66DB-4122-A3A6-7C4FFBA23BD7",
            locationName = "臺北市",
            elementName = "MinT"
        )

        pageOneViewModel.fetchData(getWeatherData)
        pageOneViewModel.run {
            weatherLiveData.observe(this@MainActivity, Observer {
                when (it) {
                    is WeatherStatus.Success<*> -> {
                        val datam  = it.data as WeatherResponse
                        val time = com.example.newapp.data.response.Time(isIcon = true)
                        for (i in 1..6 step 2) {
                            datam.records.location.first().weatherElement.first().time.add(i,time)
                        }
                        pageOneAdapter.data = datam.records.location.first().weatherElement.first().time
                        weatherList.adapter = pageOneAdapter
                    }
                    is WeatherStatus.Failure<*> ->
                        Toast.makeText(this@MainActivity, "${it.message}", Toast.LENGTH_SHORT).show()

                    is WeatherStatus.TimeOut<*> -> Toast.makeText(
                        this@MainActivity,
                        "${it.message}",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
        }
        myLamd ({
            it
        },{
            it})

        val xx = listOf<String>("1","2")
        val cc = mutableListOf<String>()
        cc.add("3")
        val dd = arrayListOf<String>()
        dd.add("4")

    }

    override fun gotoNextPage(time: com.example.newapp.data.response.Time) {
        val intent = Intent(this,Page2Activity::class.java).apply {
            putExtra("startTime",time.startTime)
            putExtra("endTime",time.endTime)
            putExtra("weather",time.parameter.parameterName)
            putExtra("weatherTwo",time.parameter.parameterUnit)
        }
        startActivity(intent)
        mapOf<String,String>("1" to "222")
        "臺北市" merge("雲林縣")
    }
    
}
