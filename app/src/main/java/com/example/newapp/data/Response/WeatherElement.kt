package com.example.newapp.data.response


import com.google.gson.annotations.SerializedName

data class WeatherElement(
    @SerializedName("elementName")
    var elementName: String = "",
    @SerializedName("time")
    var time: ArrayList<Time> = ArrayList()
)