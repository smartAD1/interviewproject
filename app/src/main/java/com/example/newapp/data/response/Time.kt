package com.example.newapp.Data.Response


import com.google.gson.annotations.SerializedName

data class Time(
    @SerializedName("endTime")
    var endTime: String = "",
    @SerializedName("parameter")
    var parameter: Parameter = Parameter(),
    @SerializedName("startTime")
    var startTime: String = "",
    var isIcon: Boolean = false
)