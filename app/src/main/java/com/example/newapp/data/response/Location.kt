package com.example.newapp.Data.Response


import com.google.gson.annotations.SerializedName

data class Location(
    @SerializedName("locationName")
    var locationName: String = "",
    @SerializedName("weatherElement")
    var weatherElement: ArrayList<WeatherElement> = ArrayList()
)