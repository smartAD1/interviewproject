package com.example.newapp.Data.Response


import com.google.gson.annotations.SerializedName

data class Result(
    @SerializedName("fields")
    var fields: List<Field> = arrayListOf(),
    @SerializedName("resource_id")
    var resourceId: String = ""
)