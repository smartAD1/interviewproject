package com.example.newapp.data.response


import com.google.gson.annotations.SerializedName

data class Records(
    @SerializedName("datasetDescription")
    var datasetDescription: String = "",
    @SerializedName("location")
    var location: ArrayList<Location> = ArrayList()
)