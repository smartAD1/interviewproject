package com.example.newapp.data.response


import com.google.gson.annotations.SerializedName

data class Result(
    @SerializedName("fields")
    var fields: List<Field> = listOf(),
    @SerializedName("resource_id")
    var resourceId: String = ""
)