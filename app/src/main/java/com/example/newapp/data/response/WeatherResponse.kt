package com.example.newapp.Data.Response


import com.google.gson.annotations.SerializedName

data class WeatherResponse(
    @SerializedName("records")
    var records: Records = Records(),
    @SerializedName("result")
    var result: Result = Result(),
    @SerializedName("success")
    var success: String = ""
)