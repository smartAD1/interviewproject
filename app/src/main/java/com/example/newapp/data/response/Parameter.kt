package com.example.newapp.Data.Response


import com.google.gson.annotations.SerializedName

data class Parameter(
    @SerializedName("parameterName")
    var parameterName: String = "",
    @SerializedName("parameterUnit")
    var parameterUnit: String = ""
)