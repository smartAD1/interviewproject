package com.example.newapp.Data.Response


import com.google.gson.annotations.SerializedName

data class Field(
    @SerializedName("id")
    var id: String = "",
    @SerializedName("type")
    var type: String = ""
)