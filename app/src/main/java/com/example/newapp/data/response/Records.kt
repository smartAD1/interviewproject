package com.example.newapp.Data.Response


import com.google.gson.annotations.SerializedName

data class Records(
    @SerializedName("datasetDescription")
    var datasetDescription: String = "",
    @SerializedName("location")
    var location: ArrayList<Location> = ArrayList()
)