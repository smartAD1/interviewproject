package com.example.newapp.Data.response.request

data class GETWeatherData(
    var apiKey: String = "",
    var locationName: String = "",
    var elementName: String = ""
)