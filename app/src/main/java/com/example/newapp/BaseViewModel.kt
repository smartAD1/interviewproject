package com.example.newapp

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
abstract class BaseViewModel: ViewModel() {
    val compositeDisposable = CompositeDisposable()


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}